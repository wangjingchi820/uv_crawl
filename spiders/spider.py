# -*- coding: utf-8 -*-
import scrapy
import xlrd
#from scrapy import Request
from scrapy.http import FormRequest
from alexa.items import AlexaItem
#import json
#import demjson
import re
from scrapy.selector import Selector
from scrapy.http import HtmlResponse
import requests
import pandas as pd 

class SpiderSpider(scrapy.Spider):
    name = 'spider'
    allowed_domains = ['alexa.chinaz.com']
#    start_urls = ['http://www/']
#    book=xlrd.open_workbook('D:\\b.xlsx')
#    book=xlrd.open_workbook('C:\\Users\\123\\Desktop\\all.xlsx')
#    sheet=book.sheet_by_name('网站')
#    sheet = book.sheet_by_index(0)
#    cols=sheet.col_values(1)
#    cols1=sheet.col_values(0)
    url='http://alexa.chinaz.com/Handlers/GetAlexaIpNumHandler.ashx'
#    url2='http://alexa.chinaz.com/Handlers/GetAlexaPvNumHandler.ashx'
    path='C:/Users/123/Desktop/carsweb_pv_uv_weight0.xlsx' #需要查询站点路径
    df=pd.read_excel(path)
    
    
    def start_requests(self):
        for i in self.df['url']:
            if len(i) > 0:
                formdata={'url':i}
                a=self.df.index[self.df['url'] == i].tolist()
                for j in a:
                
                    web=self.df['web'].loc[j]
#        for i in self.cols:
#            if len(i) > 0:
#                formdata={'url':i}
#                web=j
#        i='news.ijjnews.com'
#        formdata={'url':i}
            yield scrapy.FormRequest(url=self.url,method='POST',formdata=formdata,meta={'key1':i,'key2':web},callback=self.parse_content1,dont_filter=True)
#        yield scrapy.FormRequest(url=self.url2,method='POST',formdata=formdata,meta={'key3':i,'key4':formdata},callback=self.parse_content2,dont_filter=True)
    def parse_content1(self,response):
        urls=response.meta['key1']
#        formdata=response.meta['key2']
        web=response.meta['key2']
#        print(web)
        item=AlexaItem()
        pattern=re.compile(r'IpNum:"([0-9]*)"')
#        pattern2=re.compile(r'week:"(\b2\S*?周\b)"')
#        pattern=re.compile('[0-9]')
#        pattern=re.compile('IpNum')
#        pattern=re.compile(r'^[1-9][0-9]{0,6}$'
#        if len(pattern.findall(response.text)) ==0:
        item['web']=web
        item['url']=urls  
        if len(pattern.findall(response.text)) !=0:
            item['tuv']=pattern.findall(response.text)[7]
            item['uv_percentage']=int(1)
            item['uv']=pattern.findall(response.text)[7]
        elif len(pattern.findall(response.text)) ==0:
            patt=re.compile(r'\.(.*)')
#            patt1=re.compile(r'\.(.*)/')
            url=patt.findall(urls)
#            url1=patt1.findall(urls)
#            response1=HtmlResponse(url=url[0])
#            r=requests.post('http://alexa.chinaz.com/ijjnews.com',data=formdata)
            r=requests.get('http://alexa.chinaz.com/'+url[0])
            texts=Selector(text=r.text).xpath('//div[@class="mt1 subOlist h70 sOlist"]/ul/li[1]/text()').extract()
            if urls in texts:
                ind=texts.index(urls)
                qpuv=Selector(text=r.text).xpath('//div[@class="mt1 subOlist h70 sOlist"]/ul['+str(ind+1)+']/li[2]/text()').extract()[0]
                
#                ppv=Selector(text=r.text).xpath('//div[@class="mt1 subOlist h70 sOlist"]/ul['+str(ind+2)+']/li[3]/text()').extract()
#                tuv=pattern.findall(response.text)[7]
                formdata={'url':url[0]}
                r1=requests.post(self.url,data=formdata)
                tuv=pattern.findall(r1.text)[7]
                puv=round(float(qpuv.strip('%'))/100,3)
                item['uv_percentage']=puv
                uv=puv*int(tuv) #计算二级域名的uv
                item['tuv']=tuv
                item['uv']=uv
#            elif url1[0] in texts:
#                ind=texts.index(url[0])
#                qpuv=Selector(text=r.text).xpath('//div[@class="mt1 subOlist h70 sOlist"]/ul['+str(ind+2)+']/li[2]/text()').extract()[0]
#                ppv=Selector(text=r.text).xpath('//div[@class="mt1 subOlist h70 sOlist"]/ul['+str(ind+2)+']/li[3]/text()').extract()
#                tuv=pattern.findall(response.text)[7]
#                formdata={'url':url[0]}
#                r1=requests.post(self.url,data=formdata)
#                tuv=pattern.findall(r1.text)[7]
#                puv=round(float(qpuv.strip('%'))/100,3)
#                uv=puv*int(tuv)
#                item['uv']=uv
            
            else:
                item['uv']=''
            
#            texts=Selector(text=r.text).xpath('//*[@id="form"]/div[9]/div[2]/ul/li[1]/text()').extract()
#            print(texts)
            
        else:
            item['uv']=''
#        for i in res:
#        rem=pattern2.findall(response.text)
        yield item
        
        
#        addedjson=re.sub(r"(,?)(\w+?)\s*?:",r"\1'\2':",response.text)
#        jsonstr=addedjson.replace(",","\"")
#        dicts=json.loads(jsonstr)
#        dicts=dict(response.text)
#        text=response.text
#        json1=text.replace('[','')
#        dicts=json1.replace(']','')
#        a=json.loads(dicts)
#        print(a)
#        print(response.text)
        
#    def parse_content2(self,response):
#        urls=response.meta['key3']
#        formdata=response.meta['key2']
#        item=AlexaItem()
#        pattern=re.compile(r'PvNum:"([0-9]*)"')
#        pattern2=re.compile(r'week:"(\b2\S*?周\b)"')
#        pattern=re.compile('[0-9]')
#        pattern=re.compile('IpNum')
#        pattern=re.compile(r'^[1-9][0-9]{0,6}$'
#        if len(pattern.findall(response.text)) ==0:
#        item['url']=urls  
#        if len(pattern.findall(response.text)) !=0:
#            item['pv']=pattern.findall(response.text)[7]
#        elif len(pattern.findall(response.text)) ==0:
#            patt=re.compile(r'\.(.*)')
#            url=patt.findall(urls)
#            response1=HtmlResponse(url=url[0])
#            r=requests.post('http://alexa.chinaz.com/ijjnews.com',data=formdata)
#            r=requests.get('http://alexa.chinaz.com/ijjnews.com')
#            texts=Selector(text=r.text).xpath('//div[@class="mt1 subOlist h70 sOlist"]/ul/li[1]/text()').extract()
#            if url[0] in texts:
#                ind=texts.index(url[0])
#                qppv=Selector(text=r.text).xpath('//div[@class="mt1 subOlist h70 sOlist"]/ul['+str(ind+2)+']/li[3]/text()').extract()[0]
#                ppv=Selector(text=r.text).xpath('//div[@class="mt1 subOlist h70 sOlist"]/ul['+str(ind+2)+']/li[3]/text()').extract()
#                tuv=pattern.findall(response.text)[7]
#                formdata={'url':url[0]}
#                r1=requests.post(self.url,data=formdata)
#                tpv=pattern.findall(r1.text)[7]
#                ppv=round(float(qppv.strip('%'))/100,3)
#                pv=ppv*int(tpv)
#                item['pv']=pv
#            else:
#                item['pv']=''
            
#            texts=Selector(text=r.text).xpath('//*[@id="form"]/div[9]/div[2]/ul/li[1]/text()').extract()
#            print(texts)
            
#        else:
#            item['pv']=''
#        for i in res:
#        rem=pattern2.findall(response.text)
#        yield item
        
        
#        addedjson=re.sub(r"(,?)(\w+?)\s*?:",r"\1'\2':",response.text)
#        jsonstr=addedjson.replace(",","\"")
#        dicts=json.loads(jsonstr)
#        dicts=dict(response.text)
#        text=response.text
#        json1=text.replace('[','')
#        dicts=json1.replace(']','')
#        a=json.loads(dicts)
#        print(a)
#        print(response.text)        



        

    
        

